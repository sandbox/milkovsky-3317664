# This module contains basic integration with [Wayforpay](https://www.wayforpay.com).

The module works with [Payment](https://drupal.org/project/payment).

## Installation

1. Register sign at [Wayforpay](https://wayforpay.com) and create your own Store.
2. Activate the Wayforpay payment at /admin/config/services/payment/method/configuration/webforpay with your merchant data.
