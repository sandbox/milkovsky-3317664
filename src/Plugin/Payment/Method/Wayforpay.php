<?php

namespace Drupal\wayforpay_payment\Plugin\Payment\Method;

use Drupal\Core\Url;
use Drupal\payment\Plugin\Payment\Method\PaymentMethodInterface;
use Drupal\payment_offsite_api\Plugin\Payment\Method\PaymentMethodBaseOffsite;
use Drupal\payment_offsite_api\Plugin\Payment\Method\PaymentMethodOffsiteInterface;
use Symfony\Component\HttpFoundation\Request;
use WayForPay\SDK\Collection\ProductCollection;
use WayForPay\SDK\Credential\AccountSecretCredential;
use WayForPay\SDK\Domain\Client;
use WayForPay\SDK\Domain\Product;
use WayForPay\SDK\Endpoint\PayEndpoint;
use WayForPay\SDK\Exception\InvalidFieldException;
use WayForPay\SDK\Handler\ServiceUrlHandler;
use WayForPay\SDK\Wizard\PurchaseWizard;

/**
 * A Wayforpay payment method.
 *
 * @PaymentMethod(
 *   id = "wayforpay_payment",
 *   deriver = "\Drupal\payment_offsite_api\Plugin\Payment\Method\PaymentMethodBaseOffsiteDeriver",
 *   operations_provider = "\Drupal\payment_offsite_api\Plugin\Payment\Method\PaymentMethodBaseOffsiteOperationsProvider",
 * )
 */
class Wayforpay extends PaymentMethodBaseOffsite implements PaymentMethodOffsiteInterface {

  /**
   * {@inheritdoc}
   */
  protected function getSupportedCurrencies() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function paymentForm() {
    $form = [];
    $payment = $this->getPayment();

    $endpoint = new PayEndpoint();
    $form['#action'] = $endpoint->getUrl();

    $amount = round($payment->getAmount(), 1);
    $currency = $payment->getCurrency()->currencyCode;
    $order_id = $this->getTransactionId();
    $email = $payment->getOwner()->getEmail();
    $display_name = explode(' ', $payment->getOwner()->getDisplayName());
    $first_name = $display_name[0] ?? strstr($email, '@', TRUE);
    $last_name = $display_name[1] ?? NULL;

    // @todo Provide ability to set other client data.
    $client = new Client($first_name, $last_name, $email);
    $products = [];
    foreach ($payment->getLineItems() as $line_item) {
      $products[] = new Product($line_item->getName(), $line_item->getAmount(), $line_item->getQuantity());
    }

    $return_url = $this->pluginDefinition['config']['return_url'];
    $service_url = $this->pluginDefinition['config']['service_url'];
    $created_date = new \DateTime();
    $created_date->setTimestamp($payment->getCreatedTime());
    $merchant_data = PurchaseWizard::get($this->getCredential())
      ->setOrderReference($order_id)
      ->setOrderNo($this->getPayment()->id())
      ->setAmount($amount)
      ->setCurrency($currency)
      ->setOrderDate($created_date)
      ->setMerchantDomainName($this->pluginDefinition['config']['merchant_domain'])
      ->setClient($client)
      ->setProducts(new ProductCollection($products))
      ->setReturnUrl($this->convertToAbsoluteUrl($return_url))
      ->setServiceUrl($this->convertToAbsoluteUrl($service_url))
      ->getForm()
      ->getData();
    $merchant_data = array_filter($merchant_data);

    foreach ($merchant_data as $key => $value) {
      if (is_array($value)) {
        foreach ($value as $id => $field) {
          $this->addPaymentFormData($key . '[' . $id . ']', $field);
        }
      }
      else {
        $this->addPaymentFormData($key, $value);
      }
    }
    $form += $this->generateForm();

    return $form;
  }

  /**
   * Gets credential based on the current environment.
   *
   * @return \WayForPay\SDK\Credential\AccountSecretCredential
   *   The credential.
   */
  public function getCredential() {
    if ($this->pluginDefinition['config']['environment'] == 'live') {
      return new AccountSecretCredential(
        $this->pluginDefinition['config']['merchant_login'],
        $this->getSecretKey()
      );
    }

    // @see https://wiki.wayforpay.com/en/view/852472
    return new AccountSecretCredential(
      'test_merch_n1',
      'flk3409refn54t54t*FNJRET'
    );
  }

  /**
   * Converts internal URL to an external.
   *
   * @param string $internal_url
   *   Internal URL.
   *
   * @return string
   *   Absolute URL.
   */
  protected function convertToAbsoluteUrl($internal_url) {
    $url = Url::fromUserInput($internal_url);
    $url->setAbsolute(TRUE);
    return $url->toString();
  }

  /**
   * Returns transition id.
   *
   * @return string
   *   The transition id.
   */
  public function getTransactionId() {
    $prefix = $this->pluginDefinition['config']['order_id_prefix'] ?? '';
    return $prefix . $this->getPayment()->id();
  }

  /**
   * {@inheritdoc}
   */
  public function ipnExecute() {
    $this->logger->notice('start_exec');
    $ipn_result = [
      'status' => 'fail',
      'message' => '',
      'response_code' => 200,
    ];
    if (!$this->ipnValidate()) {
      return $ipn_result;
    }

    try {
      $handler = new ServiceUrlHandler($this->getCredential());
      $response = $handler->parseRequestFromGlobals();
    }
    catch (InvalidFieldException $e) {
      if ($this->isVerbose()) {
        $this->logger->info('Ipn failed. ' . $e->getMessage());
      }
      return $ipn_result;
    }

    $payment_status = $this->configuration['ipn_statuses'][$response->getTransaction()->getStatus()];
    $status = $payment_status ?? 'payment_pending';
    $this->getPayment()->setPaymentStatus($this->paymentStatusManager->createInstance($status));
    $this->getPayment()->save();

    return [
      'status' => 'success',
      'message' => '',
      'response_code' => 200,
    ];
  }

  /**
   * Builds payment confirmation.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param \Drupal\payment\Plugin\Payment\Method\PaymentMethodInterface $payment_method
   *   The payment method.
   *
   * @return array
   *   The confirmation render array.
   *
   * @see \Drupal\payment_offsite_api\Controller\PaymentOffsiteController::content
   */
  public function getReturnContent(Request $request, PaymentMethodInterface $payment_method) {
    $handler = new ServiceUrlHandler($this->getCredential());
    $response = $handler->parseRequestFromGlobals();
    $email = $response->getTransaction()->getEmail();
    $email_masked = $this->maskEmail($email);
    $status = $response->getTransaction()->getStatus();

    return [
      '#theme' => 'wayforpay_payment_confirmation',
      '#order_reference' => $response->getTransaction()->getOrderReference(),
      '#amount' => $response->getTransaction()->getAmount(),
      '#currency' => $response->getTransaction()->getCurrency(),
      '#email' => $email,
      '#email_masked' => $email_masked,
      '#status' => $status,
      '#text' => $this->t('Payment status is @status. Further details were send to your email address @email_masked.', [
        '@status' => $status,
        '@email_masked' => $email_masked,
      ]),
      '#response' => $response,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getResultPages() {
    return [
      'return' => TRUE,
      'service' => TRUE,
    ];
  }

  /**
   * Gets secret key.
   *
   * @return string
   *   The secret key.
   */
  public function getSecretKey() {
    return $this->pluginDefinition['config']['secret_key'];
  }

  /**
   * {@inheritdoc}
   */
  public function isConfigured() {
    return !empty($this->getSecretKey());
  }

  /**
   * {@inheritdoc}
   */
  public function ipnValidate() {
    if (!$this->validateEmpty()) {
      return FALSE;
    }

    try {
      $handler = new ServiceUrlHandler($this->getCredential());
      $response = $handler->parseRequestFromGlobals();
    }
    catch (InvalidFieldException $e) {
      if ($this->isVerbose()) {
        $this->logger->info('Payment validation failed. ' . $e->getMessage());
      }
      return FALSE;
    }

    if (!$this->validateTransactionId($response->getTransaction()->getOrderReference())) {
      return FALSE;
    }
    if (!$this->validateAmount($response->getTransaction()->getAmount())) {
      return FALSE;
    }
    if (!$this->validateCurrency($response->getTransaction()->getCurrency())) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Masks email address.
   *
   * @param $email
   *   The email.
   *
   * @return string
   *   Masked email address.
   */
  protected function maskEmail($email, $beginning_length = 2, $ending_length = 2) {
    $email_parts = explode("@",$email);
    $name = implode('@', array_slice($email_parts, 0, count($email_parts) - 1));
    $prefix = substr($name,0, $beginning_length);
    $body = str_repeat('*', strlen($name) - $beginning_length - $ending_length);
    $suffix = substr($name,strlen($name) - $ending_length, $ending_length);
    return $prefix . $body . $suffix . '@' . end($email_parts);
  }

  /**
   * Currency default validator.
   *
   * @return bool
   *   TRUE on successful validation FALSE otherwise.
   */
  protected function validateCurrency($request_currency) {
    if ($this->getPayment()->getCurrency()->currencyCode != $request_currency) {
      if ($this->isVerbose()) {
        $this->logger->error('Missing transaction id currency. POST data: <pre>@data</pre>',
          ['@data' => print_r(\Drupal::request()->request, TRUE)]
        );
      }
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Transaction ID default validator.
   *
   * @param string $order_id
   *   The order id.
   *
   * @return bool
   *   TRUE on successful validation FALSE otherwise.
   */
  protected function validateTransactionId($order_id) {
    $prefix = $this->pluginDefinition['config']['order_id_prefix'] ?? '';
    $payment_id = str_replace($prefix, '', $order_id);

    /** @var \Drupal\payment\Entity\PaymentInterface $payment */
    $payment = \Drupal::entityTypeManager()->getStorage('payment')
      ->load($payment_id);
    if (!$payment) {
      if ($this->isVerbose()) {
        $this->logger->error('Missing transaction id. POST data: <pre>@data</pre>',
          ['@data' => print_r($this->request->request, TRUE)]
        );
      }
      return FALSE;
    }
    $this->setPayment($payment);
    return TRUE;
  }

  /**
   * Amount default validator.
   *
   * @return bool
   *   TRUE on successful validation FALSE otherwise.
   */
  protected function validateAmount($request_amount) {
    if ($this->getPayment()->getAmount() != $request_amount) {
      if ($this->isVerbose()) {
        $this->logger->error('Missing transaction id amount. POST data: <pre>@data</pre>',
          ['@data' => print_r(\Drupal::request()->request, TRUE)]
        );
      }
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Empty default validator.
   *
   * @return bool
   *   TRUE on successful validation FALSE otherwise.
   */
  protected function validateEmpty() {
    // Exit now if the $_POST was empty.
    if (empty($this->request->request->all())) {
      if ($this->isVerbose()) {
        $this->logger->error('Interaction URL accessed with no POST data submitted.');
      }
      return FALSE;
    }
    return TRUE;
  }

}
