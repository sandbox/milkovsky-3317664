<?php

namespace Drupal\wayforpay_payment\Plugin\Payment\MethodConfiguration;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\payment_offsite_api\Plugin\Payment\MethodConfiguration\PaymentMethodConfigurationBaseOffsite;

/**
 * Provides the configuration for the Wayforpay payment method plugin.
 *
 * @PaymentMethodConfiguration(
 *   description = @Translation("Wayforpay payment gateway configuration."),
 *   id = "wayforpay_payment",
 *   label = @Translation("Wayforpay")
 * )
 */
class WayforpayConfiguration extends PaymentMethodConfigurationBaseOffsite implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'message_text' => 'In addition to the order amount Wayforpay fee can be charged.',
      'message_text_format' => 'plain_text',
      'ipn_statuses' => [
        'Successful' => 'payment_success',
        'Declined' => 'payment_cancelled',
        'RefundInProcessing' => 'payment_pending',
        'Refunded' => 'payment_refunded',
        'WaitingAuthComplete' => 'payment_pending',
        'InProcessing' => 'payment_pending',
        'Antifraud monitoring' => 'payment_pending',
        'Expired' => 'payment_expired',
      ],
      'config' => [
        'environment' => 'test',
        'secret_key' => '',
        'merchant_login' => '',
        'merchant_domain' => '',
        'return_url' => '/payment_offsite/{your_payment_method_machine_name}/return',
        'service_url' => '/payment_offsite/{your_payment_method_machine_name}/ipn',
        'order_id_prefix' => '',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function processBuildConfigurationForm(array &$element, FormStateInterface $form_state, array &$form) {
    $config = $this->configuration['config'];

    $element['payment'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Wayforpay settings'),
      '#tree' => TRUE,
    ];
    $element['payment']['environment'] = [
      '#type' => 'radios',
      '#title' => $this->t('Environment'),
      '#options' => [
        'test' => $this->t('Test'),
        'live' => $this->t('Live'),
      ],
      '#default_value' => $config['environment'],
      '#required' => TRUE,
    ];
    $element['payment']['secret_key'] = [
      '#type' => 'password',
      '#title' => $this->t('Secret key'),
      '#default_value' => $config['secret_key'],
      '#maxlength' => 255,
    ];
    $element['payment']['merchant_login'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant login'),
      '#default_value' => $config['merchant_login'],
      '#maxlength' => 255,
      '#required' => TRUE,
    ];
    $element['payment']['merchant_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant domain'),
      '#default_value' => $config['merchant_domain'],
      '#maxlength' => 255,
      '#required' => TRUE,
    ];
    $element['payment']['return_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Return URL'),
      '#default_value' => $config['return_url'],
      '#description' => $this->t('URL, to which the system has to transfer client with the payment result. Pay attention, that {your_payment_method_machine_name} should be replaced with the machine name of this payment method'),
      '#required' => TRUE,
    ];
    $element['payment']['service_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Service URL'),
      '#default_value' => $config['service_url'],
      '#description' => $this->t('URL, to which the system has to send a response with the payment result directly to the merchant. Pay attention, that {your_payment_method_machine_name} should be replaced with the machine name of this payment method'),
      '#required' => TRUE,
    ];
    $element['payment']['order_id_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Order ID prefix'),
      '#description' => $this->t('Additional prefix for order ID. Example: "foo -".'),
      '#default_value' => $config['order_id_prefix'],
    ];

    return parent::processBuildConfigurationForm($element, $form_state, $form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $parents = $form['plugin_form']['payment']['#parents'];
    $values = $form_state->getValues();
    $values = NestedArray::getValue($values, $parents);
    foreach ($values as $key => $value) {
      if ($key == 'secret_key' && !$value) {
        continue;
      }
      $this->configuration['config'][$key] = $value;
    }
  }

}
